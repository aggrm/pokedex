
package Pokemon;

/**
 *
 * @author Jorge Cisneros
 */
public class Pokemon {
    public String nombre ="";
    public int altura = 0;
    public int peso = 0;
    public String species="";
    public String habitad="";
    public int generation_id = 0;
    public int evolution_chain_id = 0;
    public int ratio_captura = 0;
    public int num_pokedex = 0;
}
