package Pokemon;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;



/**
 *
 * @author jorgecisneros
 */
public class VentanaPokedex extends javax.swing.JFrame {

    BufferedImage plantilla = null;
    private int contador = 0;
    private int ancho = 180, alto = 180;
    int total_pokemons = 0;
    // conectamos a la base de datos

    private Statement estado;
    private ResultSet resultadoConsulta;
    private Connection conexion;
    
 
    ////////////////////////////////////////
    
    //hashmap para almacenar el resultado de la consulta
    HashMap <String,Pokemon> listaPokemons = new HashMap();
    
    /**
     * Creates new form VentanaPokedex
     */
    private ImageIcon devuelveElPokemonQueEstaEnLaPosicion (int posicion){
        int columna = posicion / 31;
        int fila = posicion % 31;
        return ( new ImageIcon(plantilla.getSubimage(fila*96, columna*96, 96, 96)
                .getScaledInstance(ancho, alto, Image.SCALE_DEFAULT))); 
    }
    
    private ImageIcon devuelveElPokemonQueEstaEnLaPosicion2 (int posicion){
        int columna = posicion / 31;
        int fila = posicion % 31;
        return ( new ImageIcon(plantilla.getSubimage(fila*96, columna*128, 128, 128)
                .getScaledInstance(ancho, alto, Image.SCALE_DEFAULT))); 
    }
    
    private void escribeDatos(){
        Pokemon p = listaPokemons.get(String.valueOf(contador+1));
        if (p != null){
            nombreCambiante.setText(p.nombre);
            alturaCambiante.setText(String.valueOf(p.altura));
            pesoCambiante.setText(String.valueOf(p.peso));
            especieCambiante.setText(p.species);
            habitadCambiante.setText(p.habitad);
            generacionCambiante.setText(String.valueOf(p.generation_id));
            ratioCambiante.setText(String.valueOf(p.ratio_captura));
            numPokeCambiante.setText(String.valueOf(p.num_pokedex));
            
        }
        else {
            nombreCambiante.setText("No  registrado");
        }
    }

    public VentanaPokedex() {
        initComponents();
        //------------------------------Menu de la pokédex----------------------
        datos.setForeground(Color.BLACK);
        nombre.setForeground(Color.BLACK);
        nombreCambiante.setForeground(Color.BLACK);
        altura.setForeground(Color.BLACK);
        alturaCambiante.setForeground(Color.BLACK);
        peso.setForeground(Color.BLACK);
        pesoCambiante.setForeground(Color.BLACK);
        especie.setForeground(Color.BLACK);
        especieCambiante.setForeground(Color.BLACK);
        habitad.setForeground(Color.BLACK);
        habitadCambiante.setForeground(Color.BLACK);
        generacion.setForeground(Color.BLACK);
        generacionCambiante.setForeground(Color.BLACK);
        ratio.setForeground(Color.BLACK);
        ratioCambiante.setForeground(Color.BLACK);
        numPoke.setForeground(Color.BLACK);
        numPokeCambiante.setForeground(Color.BLACK);
        //----------------------------------------------------------------------
        
        setSize(748,574);
        this.setLocationRelativeTo(null);
        try{
            plantilla = ImageIO.read(getClass().getResource("black-white.png"));
            plantilla = ImageIO.read(getClass().getResource("black-white.png"));
        }
        catch (IOException e){}
   
        //conexion a la base de datos//////////////////
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://127.0.0.1/pokemon"
                    + "","root","root");
            estado = conexion.createStatement();
            resultadoConsulta = estado.executeQuery("Select * from pokemon");
            //cargo el resultado de la query en mi hashmap
            while (resultadoConsulta.next()){
                Pokemon p = new Pokemon();
                p.nombre = resultadoConsulta.getString(2);
                p.altura = resultadoConsulta.getInt(10);
                p.peso = resultadoConsulta.getInt(11);
                p.species = resultadoConsulta.getString(12);
                p.habitad = resultadoConsulta.getString(15);
                p.generation_id = resultadoConsulta.getInt(5);
                p.ratio_captura = resultadoConsulta.getInt(17);
                p.num_pokedex = resultadoConsulta.getInt(1);
                p.evolution_chain_id = resultadoConsulta.getInt(6);
                listaPokemons.put(resultadoConsulta.getString(1), p);
            }
        }
        catch (Exception e){
        }
        total_pokemons = listaPokemons.size();
        //////////////////////////////////////////////
        pokemon.setIcon(devuelveElPokemonQueEstaEnLaPosicion(0));
        escribeDatos();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        on = new javax.swing.JLabel();
        on1 = new javax.swing.JLabel();
        left2 = new javax.swing.JButton();
        left1 = new javax.swing.JButton();
        left = new javax.swing.JButton();
        rigth = new javax.swing.JButton();
        rigth1 = new javax.swing.JButton();
        rigth2 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        pokemon1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        pokemon = new javax.swing.JLabel();
        datos = new javax.swing.JLabel();
        nombre = new javax.swing.JLabel();
        nombreCambiante = new javax.swing.JLabel();
        altura = new javax.swing.JLabel();
        alturaCambiante = new javax.swing.JLabel();
        peso = new javax.swing.JLabel();
        pesoCambiante = new javax.swing.JLabel();
        especie = new javax.swing.JLabel();
        especieCambiante = new javax.swing.JLabel();
        habitad = new javax.swing.JLabel();
        habitadCambiante = new javax.swing.JLabel();
        generacion = new javax.swing.JLabel();
        generacionCambiante = new javax.swing.JLabel();
        ratio = new javax.swing.JLabel();
        ratioCambiante = new javax.swing.JLabel();
        numPoke = new javax.swing.JLabel();
        numPokeCambiante = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        pokedex = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(null);

        on.setBackground(new java.awt.Color(255, 0, 0));
        on.setFont(new java.awt.Font("Elephant", 1, 18)); // NOI18N
        on.setForeground(new java.awt.Color(250, 0, 0));
        on.setText("O F F");
        getContentPane().add(on);
        on.setBounds(602, 470, 59, 23);

        on1.setBackground(new java.awt.Color(255, 0, 0));
        on1.setFont(new java.awt.Font("Elephant", 1, 18)); // NOI18N
        on1.setForeground(new java.awt.Color(255, 0, 0));
        on1.setText("O N");
        getContentPane().add(on1);
        on1.setBounds(480, 470, 41, 23);

        left2.setBackground(new java.awt.Color(0, 153, 0));
        left2.setContentAreaFilled(false);
        left2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                left2MousePressed(evt);
            }
        });
        getContentPane().add(left2);
        left2.setBounds(440, 460, 120, 40);

        left1.setContentAreaFilled(false);
        left1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                left1MousePressed(evt);
            }
        });
        getContentPane().add(left1);
        left1.setBounds(440, 390, 60, 40);

        left.setContentAreaFilled(false);
        left.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                leftMousePressed(evt);
            }
        });
        getContentPane().add(left);
        left.setBounds(240, 430, 30, 30);

        rigth.setContentAreaFilled(false);
        rigth.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rigthMousePressed(evt);
            }
        });
        getContentPane().add(rigth);
        rigth.setBounds(300, 430, 30, 30);

        rigth1.setContentAreaFilled(false);
        rigth1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rigth1MousePressed(evt);
            }
        });
        getContentPane().add(rigth1);
        rigth1.setBounds(490, 390, 60, 40);

        rigth2.setContentAreaFilled(false);
        rigth2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rigth2MousePressed(evt);
            }
        });
        getContentPane().add(rigth2);
        rigth2.setBounds(570, 460, 120, 40);

        jPanel3.setBackground(new java.awt.Color(51, 204, 0));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        getContentPane().add(jPanel3);
        jPanel3.setBounds(430, 170, 270, 200);
        getContentPane().add(pokemon1);
        pokemon1.setBounds(90, 450, 120, 60);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(jPanel2);
        jPanel2.setBounds(80, 180, 210, 140);
        getContentPane().add(pokemon);
        pokemon.setBounds(90, 180, 210, 150);

        datos.setFont(new java.awt.Font("Elephant", 1, 14)); // NOI18N
        datos.setText("D A T O S  P O K É M O N:");
        getContentPane().add(datos);
        datos.setBounds(460, 180, 230, 21);

        nombre.setBackground(new java.awt.Color(0, 255, 0));
        nombre.setFont(new java.awt.Font("Elephant", 1, 14)); // NOI18N
        nombre.setText("N a m e:");
        getContentPane().add(nombre);
        nombre.setBounds(440, 200, 70, 20);

        nombreCambiante.setBackground(new java.awt.Color(0, 0, 0));
        nombreCambiante.setFont(new java.awt.Font("Elephant", 1, 12)); // NOI18N
        nombreCambiante.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(nombreCambiante);
        nombreCambiante.setBounds(520, 200, 160, 20);

        altura.setBackground(new java.awt.Color(0, 255, 0));
        altura.setFont(new java.awt.Font("Elephant", 1, 14)); // NOI18N
        altura.setText("H e i g h t:");
        getContentPane().add(altura);
        altura.setBounds(440, 220, 85, 20);

        alturaCambiante.setBackground(new java.awt.Color(0, 0, 0));
        alturaCambiante.setFont(new java.awt.Font("Elephant", 1, 12)); // NOI18N
        alturaCambiante.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(alturaCambiante);
        alturaCambiante.setBounds(540, 220, 70, 20);

        peso.setBackground(new java.awt.Color(0, 255, 0));
        peso.setFont(new java.awt.Font("Elephant", 1, 14)); // NOI18N
        peso.setText("W e i g h t:");
        getContentPane().add(peso);
        peso.setBounds(440, 240, 88, 20);

        pesoCambiante.setBackground(new java.awt.Color(0, 0, 0));
        pesoCambiante.setFont(new java.awt.Font("Elephant", 1, 12)); // NOI18N
        pesoCambiante.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(pesoCambiante);
        pesoCambiante.setBounds(540, 240, 50, 20);

        especie.setBackground(new java.awt.Color(0, 255, 0));
        especie.setFont(new java.awt.Font("Elephant", 1, 14)); // NOI18N
        especie.setText("S p e c i e:");
        getContentPane().add(especie);
        especie.setBounds(440, 260, 81, 20);

        especieCambiante.setBackground(new java.awt.Color(0, 0, 0));
        especieCambiante.setFont(new java.awt.Font("Elephant", 1, 12)); // NOI18N
        especieCambiante.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(especieCambiante);
        especieCambiante.setBounds(540, 260, 100, 20);

        habitad.setBackground(new java.awt.Color(0, 255, 0));
        habitad.setFont(new java.awt.Font("Elephant", 1, 14)); // NOI18N
        habitad.setText("H a b i t a d:");
        getContentPane().add(habitad);
        habitad.setBounds(440, 280, 120, 20);

        habitadCambiante.setBackground(new java.awt.Color(0, 0, 0));
        habitadCambiante.setFont(new java.awt.Font("Elephant", 1, 12)); // NOI18N
        habitadCambiante.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(habitadCambiante);
        habitadCambiante.setBounds(550, 280, 70, 20);

        generacion.setBackground(new java.awt.Color(0, 255, 0));
        generacion.setFont(new java.awt.Font("Elephant", 1, 14)); // NOI18N
        generacion.setText("G e n e r a t i o n:");
        getContentPane().add(generacion);
        generacion.setBounds(440, 300, 160, 20);

        generacionCambiante.setBackground(new java.awt.Color(0, 0, 0));
        generacionCambiante.setFont(new java.awt.Font("Elephant", 1, 12)); // NOI18N
        generacionCambiante.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(generacionCambiante);
        generacionCambiante.setBounds(590, 300, 30, 20);

        ratio.setBackground(new java.awt.Color(0, 255, 0));
        ratio.setFont(new java.awt.Font("Elephant", 1, 14)); // NOI18N
        ratio.setText("R a t i o  C a p t u r a:");
        getContentPane().add(ratio);
        ratio.setBounds(440, 320, 180, 20);

        ratioCambiante.setBackground(new java.awt.Color(0, 0, 0));
        ratioCambiante.setFont(new java.awt.Font("Elephant", 1, 12)); // NOI18N
        ratioCambiante.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(ratioCambiante);
        ratioCambiante.setBounds(620, 320, 30, 20);

        numPoke.setBackground(new java.awt.Color(0, 255, 0));
        numPoke.setFont(new java.awt.Font("Elephant", 1, 14)); // NOI18N
        numPoke.setText("N u m   I n    P o k é d e x:");
        getContentPane().add(numPoke);
        numPoke.setBounds(440, 340, 210, 20);

        numPokeCambiante.setBackground(new java.awt.Color(0, 0, 0));
        numPokeCambiante.setFont(new java.awt.Font("Elephant", 1, 12)); // NOI18N
        numPokeCambiante.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(numPokeCambiante);
        numPokeCambiante.setBounds(660, 340, 40, 20);

        jPanel1.setBackground(new java.awt.Color(51, 204, 0));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(430, 170, 270, 200);

        pokedex.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pokemon/imagenes/pokedex.png"))); // NOI18N
        pokedex.setText("jLabel3");
        getContentPane().add(pokedex);
        pokedex.setBounds(0, 10, 750, 540);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void leftMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftMousePressed
        contador--;
        if (contador < 0) {contador = 0;}
        //dibujaElPokemonQueEstaEnLaPosicion(contador);
        pokemon.setIcon(devuelveElPokemonQueEstaEnLaPosicion(contador));  
        escribeDatos();
    }//GEN-LAST:event_leftMousePressed

    private void rigthMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rigthMousePressed
        contador++;
        if (contador > total_pokemons) {contador = 0;}
        pokemon.setIcon(devuelveElPokemonQueEstaEnLaPosicion(contador));
        escribeDatos();
    }//GEN-LAST:event_rigthMousePressed

    private void rigth1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rigth1MousePressed
        contador+=15;
        if (contador > total_pokemons) 
        {
            contador = 0;
        }
        pokemon.setIcon(devuelveElPokemonQueEstaEnLaPosicion(contador));
        escribeDatos();
    }//GEN-LAST:event_rigth1MousePressed

    private void left1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_left1MousePressed
        contador-=15;
        if (contador < 0) 
        {
            contador = 0;
        }
        //dibujaElPokemonQueEstaEnLaPosicion(contador);
        pokemon.setIcon(devuelveElPokemonQueEstaEnLaPosicion(contador));
        pokemon1.setIcon(devuelveElPokemonQueEstaEnLaPosicion2(contador));
        escribeDatos();
    }//GEN-LAST:event_left1MousePressed

    private void left2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_left2MousePressed
        jPanel2.setVisible(false);
        jPanel3.setVisible(false);
    }//GEN-LAST:event_left2MousePressed

    private void rigth2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rigth2MousePressed
        jPanel2.setVisible(true);
        jPanel3.setVisible(true);
    }//GEN-LAST:event_rigth2MousePressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaPokedex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaPokedex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaPokedex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPokedex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaPokedex().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel altura;
    private javax.swing.JLabel alturaCambiante;
    private javax.swing.JLabel datos;
    private javax.swing.JLabel especie;
    private javax.swing.JLabel especieCambiante;
    private javax.swing.JLabel generacion;
    private javax.swing.JLabel generacionCambiante;
    private javax.swing.JLabel habitad;
    private javax.swing.JLabel habitadCambiante;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton left;
    private javax.swing.JButton left1;
    private javax.swing.JButton left2;
    private javax.swing.JLabel nombre;
    private javax.swing.JLabel nombreCambiante;
    private javax.swing.JLabel numPoke;
    private javax.swing.JLabel numPokeCambiante;
    private javax.swing.JLabel on;
    private javax.swing.JLabel on1;
    private javax.swing.JLabel peso;
    private javax.swing.JLabel pesoCambiante;
    private javax.swing.JLabel pokedex;
    private javax.swing.JLabel pokemon;
    private javax.swing.JLabel pokemon1;
    private javax.swing.JLabel ratio;
    private javax.swing.JLabel ratioCambiante;
    private javax.swing.JButton rigth;
    private javax.swing.JButton rigth1;
    private javax.swing.JButton rigth2;
    // End of variables declaration//GEN-END:variables
}
